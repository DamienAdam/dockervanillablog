How to
======

#### Prerequisite: 
install Docker.

#### Launch
1. clone or download repository
2. open terminal and go to folder
3. run "docker-compose build"
4. run "docker-compose up"